const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

const app = express()
const port = process.env.PORT || 4000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/users/', userRoutes)
app.use('/products/', productRoutes)

mongoose.connect(
	`mongodb+srv://ramosmark:admin123@zuitt-batch197.ixuhayw.mongodb.net/node-portfolio?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
)

const db = mongoose.connection

db.on('error', () => console.error('Connection Error'))
db.once('open', () => console.log('Connected to MongoDB'))

app.listen(port, () => console.log(`API is now online at port: ${port}`))
