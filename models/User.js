const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email is required'],
	},
	password: {
		type: String,
		required: [true, 'Password is required'],
	},
	isAdmin: {
		type: Boolean,
		default: false,
	},
	orders: [
		{
			products: {
				productName: {
					type: String,
					required: [true, 'Product Name is required'],
				},
				quantity: {
					type: Number,
					required: [true, 'Product quantity is required'],
				},
			},
			totalAmount: {
				type: Number,
				required: [true, 'Total amount of product is required'],
			},
			purchasedOn: {
				type: Date,
				default: new Date(),
			},
		},
	],
})

module.exports = mongoose.model('User', userSchema)
