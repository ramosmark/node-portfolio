const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth')

router.post('/', auth.verify, (req, res) => {
	productController.addProduct(req).then((result) => {
		res.send(result)
	})
})

router.get('/', (req, res) => {
	productController.getAllActiveProducts().then((result) => {
		res.send(result)
	})
})

router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then((result) => {
		res.send(result)
	})
})

router.put('/:productId', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		productController.updateProduct(req).then((result) => {
			res.send(result)
		})
	} else {
		return false
	}
})

router.put('/:productId/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		productController.archiveProduct(req).then((result) => {
			res.send(result)
		})
	} else {
		return false
	}
})

module.exports = router
