const jwt = require('jsonwebtoken')

const secret = 'CourseBookingAPI'

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin,
	}
	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if (token) {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return res.send({ auth: 'failed' })
			} else {
				next()
			}
		})
	} else {
		return res.send({ auth: 'failed' })
	}
}

module.exports.decode = (token) => {
	if (token) {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return null
			} else {
				return jwt.decode(token, { completE: true }.payload)
			}
		})
	} else {
		return null
	}
}
