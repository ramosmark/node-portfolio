const Product = require('../models/Product')
const auth = require('../auth')
const User = require('../models/User')

module.exports.addProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)

	return User.findById(userData.id).then((result) => {
		if (result.isAdmin) {
			const newProduct = Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
			})

			return newProduct.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return 'Product has been created'
				}
			})
		} else {
			return false
		}
	})
}

module.exports.getAllActiveProducts = () => {
	return Product.find({ isActive: true }).then((result) => result)
}

module.exports.getProduct = (reqParam) => {
	return Product.findById(reqParam.productId).then((result) => result)
}

module.exports.updateProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const productId = req.params.productId
	const reqBody = req.body

	return User.findById(userData.id).then((result) => {
		if (userData.isAdmin) {
			const updateProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price,
			}

			return Product.findByIdAndUpdate(productId, updateProduct).then(
				(updatedProduct, error) => {
					if (error) {
						return false
					} else {
						return 'Product has been successfully edited'
					}
				}
			)
		} else {
			return false
		}
	})
}

module.exports.archiveProduct = (req) => {
	const userData = auth.decode(req.headers.authorization)
	const productId = req.params.productId

	return User.findById(userData.id).then((result) => {
		if (userData.isAdmin) {
			return Product.findByIdAndUpdate(productId, { isActive: false }).then(
				(updatedProduct, error) => {
					if (error) {
						return false
					} else {
						return 'Product has been archived'
					}
				}
			)
		} else {
			return 'You are not an admin'
		}
	})
}
