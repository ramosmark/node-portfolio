const bcrypt = require('bcrypt')
const User = require('../models/User')
const auth = require('../auth')
const Product = require('../models/Product')

module.exports.registerUser = (reqBody) => {
	const newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return 'Invalid registration'
		} else {
			return 'You have been registered!'
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email }).then((result) => {
		if (result === null) {
			return 'No user has been found'
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				reqBody.password,
				result.password
			)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result) }
			} else {
				return 'Password is Incorrect'
			}
		}
	})
}

module.exports.createOrder = async (data) => {
	console.log(data)
	if (data.isAdmin) {
		return 'You cannot create an order because you are an admin'
	} else {
		const product = await Product.findById(data.productId).then(
			(product) => product
		)

		const isUserUpdated = await User.findById(data.userId).then((user) => {
			user.orders.push({
				products: {
					productName: product.name,
					quantity: data.quantity,
				},
				totalAmount: product.price * data.quantity,
			})

			return user.save().then((user, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		})

		const isProductUpdated = await Product.findById(data.productId).then(
			(product) => {
				product.orders.push({ orderId: data.userId })

				return product.save().then((product, error) => {
					if (error) {
						return false
					} else {
						return true
					}
				})
			}
		)

		if (isUserUpdated && isProductUpdated) {
			return 'User successfully ordered'
		} else {
			return `Can't process order. Error encountered`
		}
	}
}

module.exports.getDetails = (userData) => {
	return User.findById(userData.id).then((result) => {
		if (result) {
			return result
		} else {
			return false
		}
	})
}
